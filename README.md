# This is my README
Rock Paper Scissors - Java version exercise


```
#!bash

# Test
./gradlew test 

# Build
./gradlew build

# Then play the game versus computer
java -jar builds/lib/rock-paper-scissors.jar 

# Computer vs Computer
./gradlew autoplay
```