package it.strap.player.behaviors;

import static org.junit.Assert.assertTrue;
import it.strap.hands.Hand;

import java.util.Random;

import org.junit.Test;

public class ComputerPlayerBehaviorTest {

	@Test
	public void shouldReturnRandomHandFromHandsList() {

		ComputerPlayerBehavior computerPlayerBehavior = new ComputerPlayerBehavior();

		for (int i = 0; i < 100; i++) {
			
			computerPlayerBehavior.setHand(new Random().nextInt(1000));
			
			assertTrue(computerPlayerBehavior.getHand() instanceof Hand);
			
		}

	}

}
