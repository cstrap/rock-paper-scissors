package it.strap.ruleengine;

import static org.junit.Assert.assertEquals;
import it.strap.engine.RuleEngine;
import it.strap.hands.Paper;
import it.strap.hands.Rock;
import it.strap.hands.Scissors;

import org.junit.Test;

public class RuleEngineTest {

	private final RuleEngine ruleEngine = new RuleEngine();

	@Test
	public void rockShouldDefeatsScissorsAndReturnOne() {

		int expected = 1;
		int actual = ruleEngine.compare(new Rock(), new Scissors());

		assertEquals(expected, actual);

	}

	@Test
	public void scissorsShouldDefeatsPaperAndReturnOne() {

		int expected = 1;
		int actual = ruleEngine.compare(new Scissors(), new Paper());

		assertEquals(expected, actual);

	}

	@Test
	public void paperShouldDefeatsRockAndReturnOne() {

		int expected = 1;
		int actual = ruleEngine.compare(new Paper(), new Rock());

		assertEquals(expected, actual);

	}

	@Test
	public void rockShouldTiedRockAndReturnZero() {

		int expected = 0;
		int actual = ruleEngine.compare(new Rock(), new Rock());

		assertEquals(expected, actual);

	}

	@Test
	public void paperShouldTiedPaperAndReturnZero() {

		int expected = 0;
		int actual = ruleEngine.compare(new Paper(), new Paper());

		assertEquals(expected, actual);

	}

	@Test
	public void scissorsShouldTiedScissorsAndReturnZero() {

		int expected = 0;
		int actual = ruleEngine.compare(new Scissors(), new Scissors());

		assertEquals(expected, actual);

	}

	@Test
	public void rockShouldDefeatedByPaperAndReturnMinusOne() {

		int expected = -1;
		int actual = ruleEngine.compare(new Rock(), new Paper());

		assertEquals(expected, actual);

	}

	@Test
	public void paperShouldDefeatedByScissorsAndReturnMinusOne() {

		int expected = -1;
		int actual = ruleEngine.compare(new Paper(), new Scissors());

		assertEquals(expected, actual);

	}

	@Test
	public void scissorsShouldDefeatedByRockAndReturnMinusOne() {

		int expected = -1;
		int actual = ruleEngine.compare(new Scissors(), new Rock());

		assertEquals(expected, actual);

	}

}
