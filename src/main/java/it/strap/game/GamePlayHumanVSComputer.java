package it.strap.game;

import it.strap.engine.RuleEngine;
import it.strap.player.Player;
import it.strap.player.behaviors.ComputerPlayerBehavior;
import it.strap.player.behaviors.HumanPlayerBehavior;
import it.strap.player.scoreboard.ScoreBoard;

import java.util.Scanner;

public class GamePlayHumanVSComputer {

	public static void main(String[] args) {

		ScoreBoard scoreBoard = new ScoreBoard(new RuleEngine());

		Player playerOne = new Player(new HumanPlayerBehavior());

		Player playerTwo = new Player(new ComputerPlayerBehavior());
		playerTwo.setName("HAL9000");

		Scanner scanner = new Scanner(System.in);

		while (playerOne.getScore() < 3 && playerTwo.getScore() < 3) {

			playerOne.displayBehavior();
			
			String choose = scanner.nextLine();
			
			playerOne.setPlayedHand(Integer.parseInt(choose));
			playerTwo.setPlayedHand(0);
			
			scoreBoard.score(playerOne, playerTwo);

		}
		
		System.out.println("Game over");
		
		scanner.close();

	}

}
