
package it.strap.game;
import it.strap.engine.RuleEngine;
import it.strap.player.Player;
import it.strap.player.behaviors.ComputerPlayerBehavior;
import it.strap.player.scoreboard.ScoreBoard;

public class GamePlayComputerVSComputer {

	public static void main(String[] args) {

		ScoreBoard scoreBoard = new ScoreBoard(new RuleEngine());
		
		Player playerOne = new Player(new ComputerPlayerBehavior());
		Player playerTwo = new Player(new ComputerPlayerBehavior());
		
		playerOne.setName("HAL9000");
		playerTwo.setName("ZX Spectrum");
		
		while (playerOne.getScore() < 3 && playerTwo.getScore() < 3) {
						
			playerOne.setPlayedHand(0);
			playerTwo.setPlayedHand(0);
			
			scoreBoard.score(playerOne, playerTwo);
			
		}

		System.out.println("Game over");
		
	}

}
