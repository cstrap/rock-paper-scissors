package it.strap.engine;

import it.strap.hands.Hand;
import it.strap.hands.Paper;
import it.strap.hands.Rock;
import it.strap.hands.Scissors;

public class RuleEngine {

	private static final int WIN = 1;

	private static final int TIED = 0;

	private static final int LOSE = -1;

	public int compare(Hand handOne, Hand handTwo) {

		if (handOne.getClass().getName().equals(handTwo.getClass().getName())) {
			return TIED;
		}

		if ((handOne instanceof Rock && handTwo instanceof Scissors)
				|| (handOne instanceof Paper && handTwo instanceof Rock)
				|| (handOne instanceof Scissors && handTwo instanceof Paper)) {
			return WIN;
		}

		return LOSE;

	}

}
