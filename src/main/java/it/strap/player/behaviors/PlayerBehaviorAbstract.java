package it.strap.player.behaviors;

import static java.lang.String.format;
import it.strap.hands.Hand;
import it.strap.hands.Paper;
import it.strap.hands.Rock;
import it.strap.hands.Scissors;

import java.util.Arrays;
import java.util.List;

public class PlayerBehaviorAbstract implements PlayerBehavior {

	protected List<Hand> hands = Arrays.asList(new Rock(), new Paper(),
			new Scissors());

	protected Hand hand;

	public Hand getHand() {
		return hand;
	}

	public void setHand(int index) {
		this.hand = hands.get(index);
	}

	@Override
	public void displayBehavior() {

		System.out.println("Options");

		for (int i = 0; i < hands.size(); i++) {
			System.out.println(format("Press %s to play %s", i, hands.get(i)));
		}

		System.out.print("Choose: ");

	}

}
