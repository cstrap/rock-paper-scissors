package it.strap.player.behaviors;

import java.util.Random;

public class ComputerPlayerBehavior extends PlayerBehaviorAbstract {

    @Override
    public void displayBehavior() {
        throw new UnsupportedOperationException("Not supported on Computer behavior.");
    }

    @Override
    public void setHand(int hand) {
    	// Computer has its own random choose implementation based on hands list size
        setHand();
    }

    private void setHand() {
    	this.hand = hands.get(new Random().nextInt(hands.size() - 1) + 1);
    }
    
}
