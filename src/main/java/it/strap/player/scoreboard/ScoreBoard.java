package it.strap.player.scoreboard;

import static java.lang.String.format;
import it.strap.engine.RuleEngine;
import it.strap.player.Player;

public class ScoreBoard {

	private RuleEngine ruleEngine;

	public ScoreBoard(RuleEngine ruleEngine) {
		this.ruleEngine = ruleEngine;
	}

	public void score(Player playerOne, Player playerTwo) {

		int result = ruleEngine.compare(playerOne.getPlayedHand(), playerTwo.getPlayedHand());

		System.out.print(format("%s vs %s => ", playerOne.getPlayedHand(), playerTwo.getPlayedHand()));

		if (result == 1) {
			System.out.println(playerOne + " wins!");
			playerOne.addScore(1);
		}

		if (result == -1) {
			System.out.println(playerTwo + " wins!");
			playerTwo.addScore(1);
		}

		if (result == 0) {
			System.out.println("Tie game");
		}

		System.out.println(format("%s | %s", playerOne.getScore(), playerTwo.getScore()));

		System.out.println(" === === === === ");

	}
}
