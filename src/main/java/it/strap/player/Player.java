package it.strap.player;

import it.strap.hands.Hand;
import it.strap.player.behaviors.PlayerBehaviorAbstract;

public class Player {

	private final PlayerBehaviorAbstract playerBehavior;

	private int score = 0;

	private String name = this.getClass().getSimpleName();

	public Player(PlayerBehaviorAbstract playerBehavior) {
		this.playerBehavior = playerBehavior;
	}

	public int getScore() {
		return score;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addScore(int score) {
		this.score += score;
	}

	public void displayBehavior() {
		playerBehavior.displayBehavior();
	}

	public void setPlayedHand(int hand) {
		playerBehavior.setHand(hand);
	}

	public Hand getPlayedHand() {
		return playerBehavior.getHand();
	}

	@Override
	public String toString() {
		return name;
	}

}
